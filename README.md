# Zippo Lighters Price scrapping tool #

Python Scrapy project to collect information about prices for Zippo lighters in Belarus.
The results are being stored in PostgreSQL in AWS RDS server.
The data is visualized by the separate application https://bitbucket.org/valeramaniuk/price-comparison-tool/
The results can be seen at http://www.zippo-price.by

### Contact ###
Valera MANIUK valery.maniuk.55@my.csun.edu