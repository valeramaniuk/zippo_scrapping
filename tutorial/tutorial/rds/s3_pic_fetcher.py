import requests
import boto3
import logging

'''
Gets the url of a file - writes
it to the S3
and
return the full url for this file @ S3
'''



logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter =  logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler = logging.FileHandler('../logs/s3_pictures_update.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

BUKCET_NAME = 'zippopics'

def fetch_picture(url):
    try:

        if "http" not in url:
            url ="http://"+url
        response = requests.get(url)
        return response.content

    except Exception as e:
        logger.error('Unable to retrieve via http request {}, with error {}'.format(url, e))
        return None


def put_to_s3(source_url, name):

    s3 = boto3.resource('s3')
    name = name.strip().replace(' ', '_')
    contents = fetch_picture(source_url)

    if contents:
        extension = source_url.split('.')[-1]
        full_file_name = ".".join([name, extension])
        try:
            response = s3.Object(BUKCET_NAME, full_file_name).put(Body=contents)
            if response['ResponseMetadata']['HTTPStatusCode'] == 200:
                return 'https://s3-us-west-1.amazonaws.com/{}/{}'\
                    .format(BUKCET_NAME, full_file_name)
            else:
                logger.error('Unable (Response: {}) WRITE to S3 the file {} from {}'
                             .format(response['ResponseMetadata']['HTTPStatusCode'],
                                     full_file_name,
                                     source_url
                                     )
                             )
                return ''
        except Exception as e:
            logger.error('S3 Error {}'.format(e))
            return ''
    else:
        return ''
