import psycopg2
import logging
import time

from .s3_pic_fetcher import put_to_s3
from .conf import CONF

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler = logging.FileHandler('../logs/rds_table_update.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def connect():
    # connects to a specified database
    # and returns the connection object
    db_name = 'zippo'
    try:
        conn = psycopg2.connect(
                host=CONF['host'],
                port=CONF['port'],
                user=CONF['user'],
                password=CONF['password'],
                dbname=db_name)

        return conn
    except Exception as e:
        logger.critical('Unable to connect to database: {}, error: '.format(db_name, e))
        return None


def update_db(model, price, picture_url, description, lighter_url, store_name):

    new_model_success = insert_model(
            model=model,
            description=description,
            pic_url=picture_url,
        )
    # if there are no issues with that model in general
    if new_model_success:
        insert_stock(store_name, model, price, lighter_url)
        logger.info("Successfully inserted {} from {}".format(model, store_name))
        return True
    else:
        logger.error('Unable to update model {} at store: {} '.format(model, store_name))
        return False


def get_all_missing_s3_urls():
    '''checks the whole table
        and if s3_pic_url missing
        but pic_url exists
        fetches file form the web site
        and writes it to S3
        then updates the table'''
    sql = "SELECT model, pic_url" \
          " FROM display_prices_zippomodel" \
          " WHERE s3_pic_url =''" \
          " AND pic_url != '' "
    conn = connect()
    if conn:
        cur = conn.cursor()

        cur.execute(sql)

        for item in cur.fetchall():
            model = item[0]
            url = item[1]
            try:
                s3_pic_url=put_to_s3(source_url=url,
                      name=model)
                print "{} {}".format(model, s3_pic_url)
                sql_insert = """UPDATE display_prices_zippomodel
                                SET s3_pic_url = %s
                                WHERE model = %s
                            """
                cur.execute(sql_insert,(s3_pic_url, model))
                conn.commit()
            except Exception as e:
                logger.warning('BATCH Website - > S3: Unable to move {} from {}, error {}'
                               .format(model, url, e))

        conn.close()
        return True
    else:
        return False



def insert_stock(store_name, model, price, lighter_url):
    datestamp = time.strftime("%Y/%m/%d")

    sql_exists = sql_exist = """SELECT EXISTS
      (SELECT 1 FROM display_prices_stock
       WHERE zippo_model_id= %s
       AND datestamp=%s
       AND store_id = %s); """

    sql = "INSERT INTO display_prices_stock (datestamp, price, model_url, store_id, zippo_model_id)" \
          "VALUES (%s, %s, %s, %s, %s);"

    try:

        conn = connect()
        cur = conn.cursor()

        cur.execute(sql_exists, (model, datestamp, store_name))
        is_exists = cur.fetchone()[0]

        if not is_exists:

            cur.execute(sql, (datestamp, price,lighter_url, store_name, model ))
            conn.commit()
        cur.close()
        conn.close()
    except Exception as e:
        logger.error("Inserting Stock failed with an error: {}".format(e))





def insert_model(model, description, pic_url):
    """ insert a new model into the zippomodel table """
    sql_exist = """SELECT *
                   FROM display_prices_zippomodel
                   WHERE model= %s; """

    sql = """INSERT INTO display_prices_zippomodel(model, description, pic_url,s3_pic_url)
             VALUES(%s,%s, %s, %s);"""

    #initialize, not to eventually  close non-existing connection
    conn = None

    try:

        # connect to the PostgreSQL database
        conn = connect()

        if conn:
            # create a new cursor
            cur = conn.cursor()
            #check if the lighter exists already
            cur.execute(sql_exist, (model,))
            model_exists = cur.fetchone()
            s3_pic_url = ''
            if not model_exists:
                # copy the picture to s3 and get the s3 url
                # but only if there is a url for the picture
                # on the site available
                if pic_url:
                    s3_pic_url = put_to_s3(pic_url, model)
                # execute the INSERT statement
                cur.execute(sql, (model, description, pic_url, s3_pic_url))

            # if the lighter exists but is missing a pic_url or s3_pi_url
            else:
                model_exists_pic_url = model_exists[2]
                model_exists_s3_pic_url = model_exists[3]
                model_exists_description = model_exists[1]
                model_exists_modelid = model_exists[0]


                # existing lighter has pic_url but not s3_pic_ulr
                # generate s3_pic_url
                if model_exists_pic_url and not model_exists_s3_pic_url:
                    s3_pic_url = put_to_s3(model_exists_pic_url, model_exists_modelid)
                # if we have something to update:

                if description and not model_exists_description:
                    sql_update = """UPDATE display_prices_zippomodel
                                    SET description =%s
                                    WHERE model = %s"""
                    cur.execute(sql_update, (description, model ))

                if s3_pic_url and not model_exists_s3_pic_url:
                    sql_update = """UPDATE display_prices_zippomodel
                                    SET s3_pic_url =%s
                                    WHERE model = %s"""
                    cur.execute(sql_update, (s3_pic_url, model))


                # if pic_url and not model_exists_pic_url:
                sql_update = """UPDATE display_prices_zippomodel
                                SET pic_url =%s
                                WHERE model = %s"""
                cur.execute(sql_update, (pic_url, model))

            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
            conn.close()
            return True
    except Exception as e:
        logger.critical('Unable to insert/update model {} with error: {}'.format(model, e))
        if conn is not None:
            conn.close()
        return False


