from __future__ import print_function
# -*- coding: iso-8859-15 -*-
import scrapy
import re
import boto3
import time

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
prices_table = dynamodb.Table('zippo_price')

def flush_zippo_price():
    # Deletes ALL items in prices_table
    response=prices_table.scan()

    for item in response['Items']:
        prices_table.delete_item(
                Key = {

                    'model':item['model'],
                })

    while 'LastEvaluatedKey' in response:
        response=prices_table.scan()
        for item in response['Items']:
            prices_table.delete_item(
                    Key = {
                        'model':item['datetime'],
                    })
    return


def update_zippoby_price(model, price,zippoby_url):
    model = model.strip()
    datestamp = time.strftime("%Y/%m/%d")

    response = prices_table.update_item(

        Key={
            'model': model,
            'datestamp': datestamp,

        },

        UpdateExpression="set zippoby = :p,"
                         " zippoby_url = :z",

        ExpressionAttributeValues={
            ':p': price,
            ':z': zippoby_url,
        },
        ReturnValues="UPDATED_NEW"
    )

def update_padarunki_price(model, price, padarunki_url):
    model = model.strip()
    datestamp = time.strftime("%Y/%m/%d")

    response = prices_table.update_item(

        Key={
            'model': model,
            'datestamp': datestamp,
        },

        UpdateExpression="set padarunki = :p,"
                         " padarunki_url = :z",

        ExpressionAttributeValues={
            ':p': price,
            ':z': padarunki_url,
        },
        ReturnValues="UPDATED_NEW"
    )
def update_atributby_price(model, price, atribut_url):

    model = model.strip()
    datestamp = time.strftime("%Y/%m/%d")
    response = prices_table.update_item(

        Key={
            'model': model,
            'datestamp': datestamp,
        },

        UpdateExpression="set atributby = :p,"
                         " atributby_url = :z",

        ExpressionAttributeValues={
            ':p': price,
            ':z': atribut_url,
        },
        ReturnValues="UPDATED_NEW"
    )



def update_knifeworks_rds(model, description, pic_url):
    from ..rds.update_table import insert_model
    insert_model(model, description, pic_url)


def update_knifeworks_price(model, price, picture_url,
                            model_name, lighter_url):

    datestamp = time.strftime("%Y/%m/%d")
    model = model.strip()
    response = prices_table.update_item(

        Key={
            'model': model,
            'datestamp': datestamp,

        },

        UpdateExpression="set"
                         " knifeworks = :p, "
                         "picture_url = :u, "
                         "model_name = :t,"
                         "lighter_url = :r",


        ExpressionAttributeValues={
            ':p': price,
            ':u': picture_url,
            ':t': model_name,
            ':r': lighter_url,

        },
        ReturnValues="UPDATED_NEW"
    )


class Padarunki_acessories(scrapy.Spider):

    name = 'padarunki_acessories'

    def start_requests(self):
        urls = [
            'http://www.padarunki.by/catalog/123',
            ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # All acessories are on the same page
        # jusst need to extract links to the details page of each

        links = response.css('div.catalog-grid-title a::attr(href)').extract()

        for link in links:
            # "link" is a relative link
            # for politeness
            time.sleep(0.3)
            full_url = 'http://www.padarunki.by'+link
            yield scrapy.Request(url=full_url, callback=self.parse_one_acessory)

    def parse_one_acessory(self, response):
        # watch out for encoding
        # I expect some problems here, OS dependant
        price = response.css('span.old_price::text').extract_first().encode('utf8')
        model = response.css('div.main div.title::text').extract_first().encode('utf8').strip()
        not_on_stock = response.css('a.add-cart-empty-cart').extract_first()
        if not_on_stock:
            on_stock = False
        else:
            on_stock = True

        possible_models = {
                           }
        for key in possible_models:
            if key in model:
                model = possible_models[key]


        price_match = re.match(r'(\d{1,3})\s.*\s(\d\d|\d).* ', price)
        if price_match:
            price = price_match.group(1)+'.'+price_match.group(2)
        else:
            price = '0'

        print("{} {} on stock:{}\n".format(model, price, on_stock))
        # update_padarunki_price(model=model,
        #                        price=price,
        #                        padarunki_url=response.url)
























