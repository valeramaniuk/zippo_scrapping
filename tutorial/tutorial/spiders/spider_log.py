import logging


def setup_logging(module_name):

    logger = logging.getLogger(module_name)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s:%(name)% %(message)s')
    file_handler = logging.FileHandler('../logs/spider.log')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger
