from __future__ import print_function
import scrapy
import re
import time

from ..rds.update_table import update_db
from ..mapper.map_models import map_model
from .spider_log import setup_logging


class ZippoBY(scrapy.Spider):
    name = 'zippoby'
    logger = setup_logging(__name__)

    def start_requests(self):


        urls = [
            'http://www.shop.zippo.by',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_categories)

    def parse_categories(self, response):

        menu_points = response.css('div.menu_point a::attr(href)').extract()
        for point in menu_points:
            url = response.urljoin(point)
            url += "&show_all=yes"
            yield scrapy.Request(url=url, callback=self.parse_whole_page)

    def parse_whole_page(self, response):

        def model_cleanup(model):
            # removes and replaces parts of the model
            # which have to be addressed manually, mostly
            # because they contain non-English characters
            # and I cant figure out the encoding

            # remove 'Zippo' and 'zippo' (its a typo on their site)
            # from the model name
            # then remove everything that comes in the parethesis
            # since its a description in Russian
            model = model.replace('Zippo', '')
            model = model.replace('zippo', '')
            # just some garbage in descriptions
            model = model.replace('""', '')
            # Double space -> sinlge space
            model = model.replace('  ', ' ')

            # Remove Russian description
            if '(' in model:
                model = model[0:model.index('(')]

            model = model.strip()
            match = re.match(r"(\d{5}) .+", model)
            if match:
                model = match.group(1)

            return model

        def price_clean_up(price, on_stock):
            if on_stock == True:
                match = re.match(r"(\d+.\d*)", price.strip())
                if match:
                    price = match.group(0)
                else:
                    price = 0
                    self.logger.error("No pattern in PRICE for item {}".format(response.url))
            else:
                price = 0

            return price

        def pic_url_list_cleanup(pic_url_list):
            cleaned_list =[]
            for pic_url in pic_url_list:
                if 'products_pictures/' in pic_url:
                    cleaned_list.append("http://www.shop.zippo.by/"+pic_url)
            return cleaned_list

        models_list = response.css('div.product_name a::text').extract()
        prices_list = response.css('div.price::text').extract()
        on_stock_list = response.css('div.prod_').extract()
        url_list = response.css('div.product_name a::attr(href)').extract()
        pic_url_list = response.css('td a img::attr(src)').extract()

        # filters only pictures of the lighters
        pic_url_list=pic_url_list_cleanup(pic_url_list)

        for model, price, url, on_stock, pic_url in zip (models_list,
                                                prices_list,
                                                url_list,
                                                on_stock_list,
                                                pic_url_list):
            if '<a href' in on_stock:
                on_stock = True
            else:
                on_stock = False
            try:
                model = model_cleanup(model.encode('utf8'))
            except UnicodeError:
                self.logger.error("Unicode Error for the item {}".format(response.url))

            model = map_model(model)
            price = price_clean_up(price, on_stock)
            url = response.urljoin(url)
            # print ("{} {}".format(model, pic_url))
            if price != 0:
                if model is not None:
                    update_db(model=model,
                              price=price,
                              picture_url=pic_url,
                              description='',
                              lighter_url=url,
                              store_name='zippoby')
                else:
                    self.logger("Model is None for {}".format(response.url))