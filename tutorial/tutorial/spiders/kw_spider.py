from __future__ import print_function
import scrapy
import re
import time

from ..rds.update_table import update_db
from ..mapper.map_models import map_model
from .spider_log import setup_logging

class KnifeWorks(scrapy.Spider):

    name = 'knifeworksby'
    logger = setup_logging(__name__)



    def start_requests(self):
        urls = [
            'http://knifeworks.by/7-zazhigalki-zippo',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        products = response.css('div.product-container')

        for product in products:
            links = product.css('a.product-name::attr(href)').extract()
            for link in links:
                time.sleep(2)
                yield scrapy.Request(url=link, callback=self.parse_one_lighter)

        next_page = response.css('li.pagination_next a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_one_lighter(self, response):

        model = response.css('p[id=product_reference] span.editable::text').extract_first()
        price = response.css('span[id=our_price_display]::text').extract_first().replace(",", ".")
        picture_url = response.css('img[id=bigpic]::attr(src)').extract_first()
        model_name = response.css('h1[itemprop=name]::text').extract_first().encode('utf8')
        lighter_url = response.url

        # if the item is out of stock - there is a <span class ="label-danger">
        label = response.css('span.label-danger').extract_first()
        price_match = re.match(r'\d+.\d\d', price)
        if price_match:
            price = price_match.group(0)
        else:
            price = 'NOPATTERN'

        if label:
            price = 0

        model = map_model(model)
        if model is not None:
            update_db(model=model,
                      price=price,
                      picture_url=picture_url,
                      description=model_name,
                      lighter_url=lighter_url,
                      store_name='knifeworksby')
        else:
            self.logger.error("Model is None for {}".format(response.url))