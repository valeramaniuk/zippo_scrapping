from __future__ import print_function
import scrapy
import re
import time

from ..rds.update_table import update_db
from ..mapper.map_models import map_model
from .spider_log import setup_logging


class AtributoBY(scrapy.Spider):

    name = 'atributby'
    logger = setup_logging(__name__)


    def start_requests(self):
        a = open('atribut.txt','w')
        a.close()

        urls = [
            'http://atribut.by/brands/Zippo/?page_size=300&page_id=1',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_categories)

    def parse_categories(self, response):

        links = response.css('div.ok-product__title-cont a::attr(href)').extract()
        for link in links:
            # theese guys dont't play around ))
            time.sleep(3)
            yield scrapy.Request(url=response.urljoin(link), callback=self.parse_one_lighter)

    def parse_one_lighter(self, response):

        model = response.css('h6.ok-product__cart-name::text').extract_first().encode('utf8')
        price = response.css('span.ok-product__price-main::text').extract_first().encode('utf8')
        pic_url = "atribut.by" + response.css('div.carousel-inner a[target=_blank]::attr(href)').extract_first()

        model_match = re.findall(r'(\d\d+\s*\d*[\w\s]*)', model)

        if model_match:
            # print('\n\nMATCH\n\n')
            model = model_match[0].replace(' ', '')
        else:
            model = 'NOPATTERN'

        price_match = re.findall(r'(\d+,\d+)', price)
        if price_match:
            price = price_match[0].replace(',', '.')
        if model is not None:
            update_db(model=model,
                      price=price,
                      picture_url=pic_url,
                      description='',
                      lighter_url=response.url,
                      store_name='atributby')
        else:
            self.logger("Model is None for {}".format(response.url))


        a = open('atribut.txt','a')

        a.write("{} - {}  {}  \n ".format(model, price, pic_url))
        a.close()