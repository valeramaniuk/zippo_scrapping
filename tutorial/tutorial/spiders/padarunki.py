from __future__ import print_function
import scrapy
import re
import time
import logging

from ..rds.update_table import update_db
from ..mapper.map_models import map_model
from .spider_log import setup_logging



class Padarunki(scrapy.Spider):

    name = 'padarunki'
    logger = setup_logging(__name__)

    def removeNonAscii(self, s):
        try:
            ascii_only = "".join(i for i in s if ord(i) < 128)
        except Exception as e:
            # don't want to pass empty sting, just in case
            ascii_only = 'to_ascii_failed'
            self.logger.error("removeNonAscii FAILED with {}".format(e))
        return ascii_only

    def padarunki_model_mapper(self, model):
        # some Store specific mapping
        # since it is NOT IDEMPOTENT
        model = model.replace('JD', 'JD.')
        model = model.replace('BJB', 'BJB.')
        model = model.replace('HD', 'HD.')

        return map_model(model)

    def start_requests(self):

        urls = [
            'http://www.padarunki.by/catalog/93',
            'http://www.padarunki.by/catalog/103',
            ]

        for url in urls:
            # yield scrapy.Request(url="http://www.padarunki.by/product/6585", callback=self.parse_one_lighter)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # All lighters are on the same page
        # juts need to extract links to the details page of each

        links = response.css('div.catalog-grid-title a::attr(href)').extract()

        for link in links:
            # "link" is a relative link
            # for politeness
            time.sleep(0.3)
            full_url = 'http://www.padarunki.by'+link
            yield scrapy.Request(url=full_url, callback=self.parse_one_lighter)

    def parse_one_lighter(self, response):

        # watch out for encoding
        # I expect some problems here, OS dependant
        price = response.css('span.new_price::text').extract_first().strip()
        model = response.css('div.main div.title::text').extract_first().strip()
        pic_url = response.css('a.cloud-zoom img::attr(src)').extract_first()
        # If we get bytes in the response decode it with cp1251
        # Since we assume it's Russian
        # Had some issues on Mac
        if type(model) == 'bytes':
            model = model.decode('utf-8').strip()

        if type(price) == 'bytes':
            price = price.decode('utf-8').strip()

        model = self.removeNonAscii(model)
        price = self.removeNonAscii(price)

        not_on_stock = response.css('a.add-cart-empty-cart').extract_first()
        if not_on_stock:
            # if it's out of stock we dont need to know
            return
        else:
            zippo_markers = ['"Zippo"', 'Zippo', 'zippo']

            # precaution in case they won't be assigned
            # which is possible if the title has awkward format
            # for example doesn't contain "Zippo"
            zippo_index = 0
            zippo_marker = ''
            for zippo_marker in zippo_markers:
                if zippo_marker in model:
                    zippo_index = model.find(zippo_marker)
                    break


            model_match = re.match(r'(\d+\s*[A-Z]{0,3}\s*\d*)', model)

            if model_match:
                model_number = model_match.group(1)
                model_number = model_number.replace(" ", "")
                model_identifier = model[zippo_index + len(zippo_marker):].strip()

                if len(model_number) == 3:
                    model_number += " " + model_identifier
            else:
                model_number = 'UNKNOWN'



            model_number = self.padarunki_model_mapper(model_number)

            price_match = re.match(r'(\d\d\d|\d\d)\s.*\s(\d\d|\d).* ', price)
            if price_match:
                price = price_match.group(1)+'.'+price_match.group(2)
            else:
                price = '0'


            if model is not None:
                update_db(model=model_number,
                          price=price,
                          picture_url=pic_url,
                          description='',
                          lighter_url=response.url,
                          store_name='padarunki')
            else:
                self.logger("Model is None for {}".format(response.url))