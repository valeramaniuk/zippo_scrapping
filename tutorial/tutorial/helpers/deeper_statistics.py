from __future__ import print_function

import statistics
import boto3
import time

import datetime
from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
zippo_price_table = dynamodb.Table('zippo_price')
zippo_housekeeping_table = dynamodb.Table('zippo_housekeeping')


def find_price_change(today_str):

    def difference_between_days(shop_name, item_yesterday, item_today):
        if shop_name in item_today and shop_name in item_yesterday:
                if item_yesterday[shop_name] != item_today[shop_name]:
                    if item_today[shop_name] == 0:
                        print("{}: {} продана {}".format(shop_name,
                                                         item_today['model'],
                                                         item_yesterday[shop_name] ))
                    elif item_yesterday[shop_name] == 0:
                        print("{}: {} завоз {}".format(shop_name,
                                                           item_today['model'],
                                                           item_today[shop_name]))
                    else:
                        print("{}: {} изменение цены {:.2f}".format(
                                        shop_name,
                                        item_today['model'],
                                        float(item_today[shop_name]) -
                                        float(item_yesterday[shop_name])))
    today_datetime_obj = datetime.datetime.strptime(today_str, "%Y/%m/%d")
    today_str = today_datetime_obj.strftime("%Y/%m/%d")

    yesterday_datetime_obj = today_datetime_obj - datetime.timedelta(1)
    yesterday_str =yesterday_datetime_obj.strftime("%Y/%m/%d")

    response_today = zippo_price_table.query(
            KeyConditionExpression=Key('datestamp').eq(today_str)
    )
    # response_yesterday = response_yesterday = zippo_price_table.query(
    #         KeyConditionExpression=Key('datestamp').eq(yesterday_str)
    # )

    for item_today in response_today['Items']:
        item_yesterday = zippo_price_table.get_item(
            Key ={'datestamp':yesterday_str,
                  'model': item_today['model']}
        )


        if 'Item' in item_yesterday:
            item_yesterday = item_yesterday['Item']
            difference_between_days('zippoby', item_yesterday, item_today)
            difference_between_days('padarunki', item_yesterday, item_today)
            difference_between_days('knifeworks', item_yesterday, item_today)
            # if 'zippoby' in item_today and 'zippoby' in item_yesterday:
            #     if item_yesterday['zippoby'] != item_today['zippoby']:
            #         print('ZippoBY: ', item_yesterday['zippoby'], item_today['zippoby'])
            #
            # if 'padarunki' in item_today and 'padarunki' in item_yesterday:
            #                 if item_yesterday['padarunki'] != item_today['padarunki']:
            #                     print('padarunki: ', item_yesterday['padarunki'], item_today['padarunki'])
            #
            # if 'knifeworks' in item_today and 'knifeworks' in item_yesterday:
            #                 if item_yesterday['knifeworks'] != item_today['knifeworks']:
            #                     print('knifeworks: ', item_yesterday['knifeworks'], item_today['knifeworks'])





find_price_change('2017/02/20')