from __future__ import print_function
# -*- coding: iso-8859-15 -*-
import statistics

import scrapy
import re
import boto3
import time

from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
zippo_price_table = dynamodb.Table('zippo_price')
zippo_housekeeping_table = dynamodb.Table('zippo_housekeeping')


def set_last_update_date(date_string):
    response = zippo_housekeeping_table.update_item(
    Key={
        'attr_name': 'Last Crawled Date',
    },
    UpdateExpression="set last_updated = :a",
    ExpressionAttributeValues={
        ':a': date_string,
    },
    ReturnValues="UPDATED_NEW"
)

def update_statistics_table(total_lighters_zippoby,
                            total_lighters_knifeworks,
                            total_lighters_atributby,
                            total_lighters_padarunki,

                            number_of_cheaper_lighters_on_zippoby,
                            number_of_cheaper_lighters_on_padarunki,
                            number_of_cheaper_lighters_on_atribut,

                            median_zippoby_absolute_diff,
                            median_zippoby_percent_diff,
                            median_padarunki_absolute_diff,
                            median_padarunki_percent_diff,
                            median_atribut_absolute_diff,
                            median_atribut_percent_diff,

                            max_absolute_diff_zippoby,
                            max_absolute_diff_padarunki,
                            max_percent_diff_zippoby,
                            max_percent_diff_padarunki,
                            max_percent_diff_atribut,
                            max_absolute_diff_atribut,

                            number_of_more_expensive_on_zippoby ,
                            number_of_more_expensive_on_padarunki,
                            number_of_more_expensive_on_atribut,

                            ):
    record_type = 'Daily summary'
    datestamp = time.strftime("%Y/%m/%d")
    set_last_update_date(datestamp)

    response = zippo_price_table.update_item(

        Key={
            'datestamp': datestamp,
            'model': record_type,

        },

        UpdateExpression="set total_lighters_zippoby = :totalZ,"
                         " total_lighters_knifeworks = :totalK,"
                         " total_lighters_atributby = :totalA,"
                         " total_lighters_padarunki = :totalP,"
                         " number_of_cheaper_lighters_on_zippoby = :cheaperZ,"
                         " number_of_cheaper_lighters_on_padarunki = :cheaperP,"
                         " number_of_cheaper_lighters_on_atribut = :cheaperA,"
                         " median_zippoby_absolute_diff = :medianabsoluteZ,"
                         " median_atribut_absolute_diff = :medianabsoluteA,"
                         " median_padarunki_absolute_diff = :medianabsoluteP,"
                         " median_zippoby_percent_diff = :medianpercentZ,"
                         " median_padarunki_percent_diff = :medianpercentP,"
                         " median_atribut_percent_diff = :medianpercentA,"
                         " max_absolute_diff_zippoby = :maxabsoluteZ,"
                         " max_absolute_diff_padarunki = :maxabsoluteP,"
                         " max_absolute_diff_atribut = :maxabsoluteA,"
                         " max_percent_diff_zippoby = :maxpercentZ,"
                         " max_percent_diff_padarunki = :maxpercentP,"
                         " max_percent_diff_atribut = :maxpercentA,"
                         " number_of_more_expensive_on_zippoby = :expensiveZ,"
                         " number_of_more_expensive_on_padarunk = :expensiveP,"
                         " number_of_more_expensive_on_atribut = :expensiveA"
                                                                ,
        ExpressionAttributeValues={
            ':totalZ': total_lighters_zippoby,
            ':totalK': total_lighters_knifeworks,
            ':totalA': total_lighters_atributby,
            ':totalP': total_lighters_padarunki,

            ':cheaperZ': number_of_cheaper_lighters_on_zippoby,
            ':cheaperP': number_of_cheaper_lighters_on_padarunki,
            ':cheaperA': number_of_cheaper_lighters_on_atribut,

            ':expensiveZ': number_of_more_expensive_on_zippoby ,
            ':expensiveP': number_of_more_expensive_on_padarunki,
            ':expensiveA': number_of_more_expensive_on_atribut,

            ':medianabsoluteZ': "{0:.2f}".format(median_zippoby_absolute_diff),
            ':medianabsoluteA': "{0:.2f}".format(median_atribut_absolute_diff),
            ':medianabsoluteP': "{0:.2f}".format(median_padarunki_absolute_diff),

            ':medianpercentZ': "{0:.2f}".format(median_zippoby_percent_diff),
            ':medianpercentP': "{0:.2f}".format(median_padarunki_percent_diff),
            ':medianpercentA': "{0:.2f}".format(median_atribut_percent_diff),

            ':maxabsoluteZ': "{0:.2f}".format(max_absolute_diff_zippoby),
            ':maxabsoluteP': "{0:.2f}".format(max_absolute_diff_padarunki),
            ':maxabsoluteA': "{0:.2f}".format(max_absolute_diff_atribut),

            ':maxpercentZ': "{0:.2f}".format(max_percent_diff_zippoby),
            ':maxpercentP': "{0:.2f}".format(max_percent_diff_padarunki),
            ':maxpercentA': "{0:.2f}".format(max_percent_diff_atribut),

        },
        ReturnValues="UPDATED_NEW"
    )

def launch_statistic_update():
    print('Statistic update in progress ...')
    total_lighters_zippoby = 0
    total_lighters_knifeworks = 0
    total_lighters_atributby = 0
    total_lighters_padarunki = 0

    number_of_cheaper_lighters_on_zippoby = 0
    number_of_cheaper_lighters_on_padarunki = 0
    number_of_cheaper_lighters_on_atribut = 0

    number_of_more_expensive_on_zippoby = 0
    number_of_more_expensive_on_padarunki = 0
    number_of_more_expensive_on_atribut = 0

    price_diff_absolute_zippoby_list = []
    price_diff_percent_zippoby_list = []
    price_diff_absolute_atribut_list = []
    price_diff_percent_atribut_list = []
    price_diff_percent_padarunki_list = []
    price_diff_absolute_padarunki_list = []

    def calculate_statistics(response):
        nonlocal total_lighters_zippoby
        nonlocal total_lighters_knifeworks
        nonlocal total_lighters_atributby
        nonlocal total_lighters_padarunki

        nonlocal number_of_cheaper_lighters_on_zippoby
        nonlocal number_of_cheaper_lighters_on_padarunki
        nonlocal number_of_cheaper_lighters_on_atribut

        nonlocal number_of_more_expensive_on_zippoby
        nonlocal number_of_more_expensive_on_padarunki
        nonlocal number_of_more_expensive_on_atribut

        nonlocal price_diff_absolute_zippoby_list
        nonlocal price_diff_percent_zippoby_list
        nonlocal price_diff_absolute_atribut_list
        nonlocal price_diff_percent_atribut_list
        nonlocal price_diff_percent_padarunki_list
        nonlocal price_diff_absolute_padarunki_list


        for item in response['Items']:

            if 'knifeworks' in item:
                knifeworks_price = float(item['knifeworks'])
                if knifeworks_price != 0:
                    total_lighters_knifeworks += 1
            else:
                knifeworks_price = 0

            if 'zippoby' in item:
                zippoby_price = float(item['zippoby'])
                if zippoby_price !=0:
                    total_lighters_zippoby += 1
            else:
                zippoby_price = 0

            if 'padarunki' in item:
                padarunki_price = float(item['padarunki'])
                if padarunki_price !=0:
                    total_lighters_padarunki += 1
            else:
                padarunki_price = 0

            if 'atributby' in item:
                atributby_price = float(item['atributby'])
                if atributby_price !=0:
                    total_lighters_atributby += 1
            else:
                atributby_price = 0

            if knifeworks_price != 0:

                if zippoby_price != 0:
                    zippoby_absolute_price_difference = zippoby_price - knifeworks_price
                    zippoby_percent_price_difference = (zippoby_price / knifeworks_price - 1) * 100
                    price_diff_absolute_zippoby_list.append(zippoby_absolute_price_difference)
                    price_diff_percent_zippoby_list.append(zippoby_percent_price_difference)
                # Counts how many pieces on zippo.by are cheaper than
                    #   on knifeworks.by
                    if zippoby_absolute_price_difference < 0:
                        number_of_cheaper_lighters_on_zippoby += 1
                    else:
                        number_of_more_expensive_on_zippoby +=1
                else:
                    zippoby_percent_price_difference = 0
                    zippoby_absolute_price_difference = 0

                if padarunki_price != 0:
                    padarunki_percent_price_difference = (padarunki_price / knifeworks_price - 1) * 100
                    padarunki_absolute_price_difference = padarunki_price - knifeworks_price
                    price_diff_absolute_padarunki_list.append(padarunki_absolute_price_difference)
                    price_diff_percent_padarunki_list.append(padarunki_percent_price_difference)

                    if padarunki_absolute_price_difference < 0:
                            number_of_cheaper_lighters_on_padarunki += 1
                    else:
                        number_of_more_expensive_on_padarunki += 1
                else:
                    padarunki_percent_price_difference = 0
                    padarunki_absolute_price_difference = 0

                if atributby_price != 0:
                    atribut_absolute_price_diff = atributby_price - knifeworks_price
                    atribut_percent_price_diff = (atributby_price / knifeworks_price - 1) * 100
                    price_diff_absolute_atribut_list.append(atribut_absolute_price_diff)
                    price_diff_percent_atribut_list.append(atribut_percent_price_diff)

                    if atribut_absolute_price_diff < 0:
                                number_of_cheaper_lighters_on_atribut += 1
                    else:
                        number_of_more_expensive_on_atribut += 1
                else:
                    atribut_absolute_price_diff = 0
                    atribut_percent_price_diff = 0

    datestamp = time.strftime("%Y/%m/%d")
    fe = Key('datestamp').eq(datestamp)

    response = zippo_price_table.scan(
            FilterExpression=fe,
    )
    calculate_statistics(response)

    while 'LastEvaluatedKey' in response:

        response = zippo_price_table.scan(
            FilterExpression=fe,
        )
        calculate_statistics(response)

    if price_diff_absolute_zippoby_list:
        median_zippoby_absolute_diff = statistics.median(price_diff_absolute_zippoby_list)
        max_absolute_diff_zippoby = max(price_diff_absolute_zippoby_list)
    else:
        median_zippoby_absolute_diff =0
        max_absolute_diff_zippoby =0

    if price_diff_percent_zippoby_list:
        median_zippoby_percent_diff = statistics.median(price_diff_percent_zippoby_list)
        max_percent_diff_zippoby = max(price_diff_percent_zippoby_list)
    else:
        median_zippoby_percent_diff = 0
        max_percent_diff_zippoby = 0

    if price_diff_absolute_padarunki_list:
        median_padarunki_absolute_diff = statistics.median(price_diff_absolute_padarunki_list)
        max_absolute_diff_padarunki = max(price_diff_absolute_padarunki_list)
    else:
        median_padarunki_absolute_diff = 0
        max_absolute_diff_padarunki = 0

    if price_diff_percent_padarunki_list:
        median_padarunki_percent_diff = statistics.median(price_diff_percent_padarunki_list)
        max_percent_diff_padarunki = max(price_diff_percent_padarunki_list)
    else:
        median_padarunki_percent_diff = 0
        max_percent_diff_padarunki = 0

    if price_diff_absolute_atribut_list:
        median_atribut_absolute_diff = statistics.median(price_diff_absolute_atribut_list)
        max_absolute_diff_atribut = max(price_diff_absolute_atribut_list)
    else:
        median_atribut_absolute_diff = 0
        max_absolute_diff_atribut = 0

    if price_diff_percent_atribut_list:
        median_atribut_percent_diff = statistics.median(price_diff_percent_atribut_list)
        max_percent_diff_atribut = max(price_diff_percent_atribut_list)
    else:
        median_atribut_percent_diff = 0
        max_percent_diff_atribut = 0


    update_statistics_table(
        total_lighters_zippoby=total_lighters_zippoby,
        total_lighters_knifeworks=total_lighters_knifeworks,
        total_lighters_atributby=total_lighters_atributby,
        total_lighters_padarunki=total_lighters_padarunki,

        number_of_cheaper_lighters_on_zippoby =number_of_cheaper_lighters_on_zippoby,
        number_of_cheaper_lighters_on_padarunki=number_of_cheaper_lighters_on_padarunki,
        number_of_cheaper_lighters_on_atribut=number_of_cheaper_lighters_on_atribut,

        median_zippoby_absolute_diff=median_zippoby_absolute_diff,
        median_zippoby_percent_diff=median_zippoby_percent_diff,
        median_padarunki_absolute_diff=median_padarunki_absolute_diff,
        median_padarunki_percent_diff=median_padarunki_percent_diff,
        median_atribut_absolute_diff=median_atribut_absolute_diff,
        median_atribut_percent_diff=median_atribut_percent_diff,

        max_absolute_diff_zippoby=max_absolute_diff_zippoby,
        max_absolute_diff_padarunki=max_absolute_diff_padarunki,
        max_percent_diff_zippoby=max_percent_diff_zippoby,
        max_percent_diff_padarunki=max_percent_diff_padarunki,
        max_percent_diff_atribut=max_percent_diff_atribut,
        max_absolute_diff_atribut=max_absolute_diff_atribut,

        number_of_more_expensive_on_zippoby=number_of_more_expensive_on_zippoby,
        number_of_more_expensive_on_padarunki=number_of_more_expensive_on_padarunki,
        number_of_more_expensive_on_atribut=number_of_more_expensive_on_atribut,
        )

if __name__ == "__main__":
    launch_statistic_update()